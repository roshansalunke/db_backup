module.exports = {
    errorHandling: async (error, req, res, next) => {
        const statusCode = error.statusCode || 500

        if (process.env.NODE_ENV != 'production') {
            return res.status(statusCode).send({
                status: "error",
                code: statusCode,
                message: error.message,
                stack: error.stack
            });
        } else {
            if (statusCode !== 500) {
                return res.status(statusCode).send({
                    status: "error",
                    code: statusCode,
                    message: error.message,
                });
            } else {
                return res.status(statusCode).send({
                    status: "error",
                    code: statusCode,
                    message: "something went wrong",
                });
            }
        }
    }
}