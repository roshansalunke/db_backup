module.exports = app => {
    const HomeController = require("../controllers/HomeController");
   
    const router = require("express").Router();

    router.post("/welcome", HomeController.welcome); //done
    // router.post("/login", AuthController.login); //done
    // router.post("/verify-email", AuthController.verify); //done
    // router.post("/forget-password", AuthController.forgotPassword);//done
    // router.post("/checkUserSecurityAnswer", AuthController.checkUserSecurityAnswer);//done
    // router.post("/createForgetPasswordrequest", AuthController.createForgetPasswordRequest);//?remove
    // router.post("/reset-password", AuthController.resetPassword);//done 
    // router.post("/change-password", AuthController.changePassword); //done
    // router.post("/updateUserProfile", AuthController.updateUserProfile); //done

    app.use("/api/", router);
}