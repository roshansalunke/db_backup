module.exports = app => {
    const NosqlController = require("../controllers/NosqlController");
   
    const router = require("express").Router();

    router.post("/mongo_bkp", NosqlController.backup); //done
    // router.post("/login", AuthController.login); //done
    // router.post("/verify-email", AuthController.verify); //done
    // router.post("/forget-password", AuthController.forgotPassword);//done
    // router.post("/checkUserSecurityAnswer", AuthController.checkUserSecurityAnswer);//done
    // router.post("/createForgetPasswordrequest", AuthController.createForgetPasswordRequest);//?remove
    // router.post("/reset-password", AuthController.resetPassword);//done 
    // router.post("/change-password", AuthController.changePassword); //done
    // router.post("/updateUserProfile", AuthController.updateUserProfile); //done

    app.use("/api/rdbms", router);
}