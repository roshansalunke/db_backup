module.exports = app => {

    //? created Route And API
    require("../routes/HomeRoute")(app);
    require("../routes/RdbmsRoute")(app);
    require("../routes/NosqlRoute")(app);
}