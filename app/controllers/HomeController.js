exports.welcome = async (req, res, next) => {
    try {
        return res.send({
          status: "success",
          code: 200,
          message: "Welcome to Home page",
        });
    } catch (error) {
      next(error);
    }
  };