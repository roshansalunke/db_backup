// const mysqlBackup = require('mysql-backup');
const fs = require('fs')
// const mysqlDump = require('mysqldump')
const mysqldump = require('mysqldump')
// const spawn = require('child_process').spawn
// const dumpFileName = `${Math.round(Date.now() / 1000)}.dump.sql`
// // const cron = require('node-cron')
// const moment = require('moment')
// // const fs = require('fs')
// // const spawn = require('child_process').spawn




exports.mysql = async (req, res, next) => {
  try {

    var userName = req.body.userName
    var pass = req.body.pass
    var dbName = req.body.dbName

    const now = new Date();
    const dateString = now.toJSON().substring(0, 16).replace(":", "");
    const filename = `db-${dateString}.sql`;
    const path = "/var/www/html/backup/" + filename;

    mysqldump({
      connection: {
          host: 'homboltadmin.hombolttech.dev',
          user: userName,
          password: pass,
          database: dbName,
      },
      dumpToFile: path,
  });


    // mysqlBackup({
    //   host: 'localhost',
    //   user: userName,
    //   password: pass,
    //   database: dbName,
    // }).then(dump => {
    //   console.log(dump);
    //   fs.writeFileSync(`${path}`, dump);
    // })

    // const fileName = `${dbName}_${moment().format('YYYY_MM_DD')}.sql`
    // const wstream = fs.createWriteStream(`/tmp/backup/${fileName}`)
    // console.log('---------------------')
    // const mysqldump = spawn('mysqldump', ['-u', userName, `-p${pass}`, dbName])
    // const path = `/tmp/backup/${fileName}`

    // mysqldump
    //   .stdout
    //   .pipe(wstream)
    //   .on('finish', () => {
    //     console.log('DB Backup Completed!')
    //     return res.send({
    //       status: "success",
    //       code: 200,
    //       message: "Backup Completed",
    //       path: path,
    //     });
    //   })
    //   .on('error', (err) => {
    //     console.log(err)
    //   })
  } catch (error) {
    next(error);
  }
};