const express = require("express");
const app = express();
const fileUpload = require("express-fileupload");
// const bodyParser = require('body-parser');
const cors = require("cors");
require("dotenv").config();
// const db = require("./app/model/index.js");

const AppError = require("./app/utils/appError.js");
const { errorHandling } = require("./app/library/ErrorHandlingLibrary.js");

var corsOptions = {
  origin: [
    "https://hom-685223600.hombolttech.dev",
    "http://192.168.1.63:5005",
    "http://192.168.1.64:5005",
    "http://localhost:5005",
    "http://localhost:8002",
    "http://192.168.1.131:5005",
    "http://192.168.1.134:5005",
    "http://192.168.1.134:4200",
    "http://192.168.1.132:5005",
    //live URL
    "https://yachterly.com",
    "https://hom-685223600.hombolttech.dev",
  ],
  methods: ["GET", "POST"],
  allowedHeaders: ["Content-Type"],
};

app.use(cors(corsOptions));
app.use(
  express.json({
    limit: "1000mb",
    extended: true,
    parameterLimit: 500000,
  })
);
app.use(
  express.urlencoded({
    extended: true,
    limit: "1000mb",
    parameterLimit: 500000,
  })
);

app.use(
  fileUpload({
    // useTempFiles: true,
    // tempFileDir: "tmp",
  })
);

app.get("/api", (req, res, next) => {
  try {
    db.authenticate().then(() => {
      return res.send({
        status: "success",
        code: "200",
        message: "Welcome to Yachterly api",
      });
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
});

require("./app/config/RouteConfig")(app);
app.use(express.static(__dirname + "/app/uploads/"));

const PORT = process.env.PORT;

app.listen(PORT, () => {
  console.log(`DB_BACKUP server runnig on http://localhost:${PORT} .`);
});

app.all("*", (req, res) => {
  throw new AppError(
    `You reached a route ${req.path} that is not defined on this server`,
    404
  );
});

app.use(errorHandling);
